**Get started**

1. Clone repository: git clone https://<user>@bitbucket.org/alexgallen/protractor-test.git
2. Install Node and npm: http://blog.teamtreehouse.com/install-node-js-npm-windows
3. Navigate to root folder of protractor-test.
4. Install protractor: npm install -g protractor
5. Install jasmine-spec-reporter: npm install jasmine-spec-reporter

**Running tests**

1. From project root: protractor conf.js

**Edit test to be run**

Edit/add specs to "specs:" in conf.js (row 8)
Example: specs: ['newtest.js']

**More information about existing test**
https://www.protractortest.org/#/tutorial
