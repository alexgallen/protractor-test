var JasmineSpecReporter = require('jasmine-spec-reporter').SpecReporter;
// conf.js
exports.config = {
    framework: 'jasmine2',
    directConnect: true,

    // CHANGE TEST TO BE RUN HERE
    specs: ['spec.js'],


    // Options to be passed to jasmine2
    jasmineNodeOpts: {
        // If true, print colors to the terminal.
        showColors: true,
        // Function called to print jasmine results.
        print: function () {
        }
    },
    onPrepare: function () {
        // 'jasmine-spec-reporter' for console report
        jasmine.getEnv().addReporter(new JasmineSpecReporter({
            suite: {
                displayNumber: true    // display each suite number (hierarchical)
            },
            spec: {
                displayStacktrace: true,    // display stacktrace for each failed assertion, values: (all|specs|summary|none)
                displayFailed: true,      // display each failed spec
                displayPending: true,    // display each pending spec
                displayDuration: true,   // display each spec duration
                displaySuccessful: true  // display each successful spec
            },
            summary: {
                displayFailed: true // display summary of all failures after execution
            },
            colors: {
                successful: 'green',
                failed: 'red',
                pending: 'cyan'
            },
            prefixes: {
                successful: '✓ ',
                failed: '✗ ',
                pending: '- '
            },
            customProcessors: []
        }));
    }
}